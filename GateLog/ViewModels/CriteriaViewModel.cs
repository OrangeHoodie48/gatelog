﻿using GateLog.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class CriteriaViewModel : TimeViewModel
    {
        private string CarrierName = "";
        public string Line { get; set; }
        public bool WantsCSV { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public SearchPageCompanyEnum Company { get; set; }
        public string Tractor { get; set; }
        public string Carrier
        {
            get
            {
                if (!CarrierName.Equals(""))
                {
                    return CarrierName;
                }
                return null;
            }
            set {
                if (value != null)
                {
                    CarrierName = value.ToUpper();
                }
            }
        }
        public string Trailer { get; set; }
        public string Trip { get; set; }
        public string Year1 { get; set; }
        public string Month1 { get; set; }
        public string Day1 { get; set; }
        public string Hour1 { get; set; }
        public string Minute1 { get; set; }
        public string Year2 { get; set; }
        public string Month2 { get; set; }
        public string Day2 { get; set; }
        public string Hour2 { get; set; }
        public string Minute2 { get; set; }


        public bool DateOrTimeError { get; set; }
        public bool TripError { get; set; }
        public bool NoFieldError { get; set; }
        public bool NoResults { get; set; }
        public bool ToIsBeforeFrom { get; set; }

        public DateTime DateAndTime { get; set; }

        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Hour { get; set; }
        public string Minute { get; set; }

        public bool SecondPassConvertDateToTime { get; set; }

        public void AddZeros()
        {
            if (Day1.Length == 1)
            {
                Day1 = "0" + Day1;
            }
            if (Month1.Length == 1)
            {
                Month1 = "0" + Month1;
            }
            if (Hour1.Length == 1)
            {
                Hour1 = "0" + Hour1;
            }
            if (Minute1.Length == 1)
            {
                Minute1 = "0" + Minute1;
            }
            if (Day2.Length == 1)
            {
                Day2 = "0" + Day2;
            }
            if (Month2.Length == 1)
            {
                Month2 = "0" + Month2;
            }
            if (Hour2.Length == 1)
            {
                Hour2 = "0" + Hour2;
            }
            if (Minute2.Length == 1)
            {
                Minute2 = "0" + Minute2;
            }
        }

        public void PrimeFromForConversion()
        {
            Year = Year1;
            Day = Day1;
            Month = Month1;
            Hour = Hour1;
            Minute = Minute1;
        }

        public void PrimeToForConversion()
        {
            Year = Year2;
            Day = Day2;
            Month = Month2;
            Hour = Hour2;
            Minute = Minute2;
        }

        public void LoadCriteriaChart(CriteriaChart criteria)
        {
            criteria.From = From;
            criteria.To = new DateTime(To.Year, To.Month, To.Day, To.Hour, To.Minute, 59, To.Millisecond);
            criteria.Trip = Trip;
            criteria.Tractor = Tractor;
            criteria.Trailer = Trailer;
            criteria.Carrier = Carrier;
            criteria.Company = Company;
        }

        public bool IsNotEmpty()
        {
            if ((Year1 == null && Year2 == null) && (Month1 == null && Month2 == null) &&
                (Day1 == null && Day2 == null) && (Hour1 == null && Hour2 == null) &&
                 (Minute1 == null && Minute2 == null) && Carrier == null && Tractor == null &&
                 Trip == null && Trailer == null && Company == SearchPageCompanyEnum.SELECT)
            {
                return false;
            }
            return true;

        }

        public void CheckAndResolveEmptyTimeFields()
        {
            if ((Year1 == null || Year2 == null) || (Month1 == null || Month2 == null) ||
                (Day1 == null || Day2 == null) || (Hour1 == null || Hour2 == null) ||
                 (Minute1 == null || Minute2 == null))
            {
                Year1 = "2018";
                Month1 = "1";
                Day1 = "1";
                Hour1 = "0";
                Minute1 = "0";

                Year2 = DateTime.Now.Year + "";
                Month2 = DateTime.Now.Month + "";
                Day2 = DateTime.Now.Day + "";
                Hour2 = DateTime.Now.Hour + "";
                Minute2 = DateTime.Now.Minute + "";
            }
            else
            {
                AddZeros();
            }

        }

        public bool FromDoesNotEqualsTo()
        {
            if (Year1.Equals(Year2) && Month1.Equals(Month2) && Day1.Equals(Day2) &&
                Hour1.Equals(Hour2) && Minute1.Equals(Minute2))
            {
                return false;
            }
            return true;
        }

        public bool TimeIsIncomplete()
        {
            if (Year1 != null || Month1 != null || Day1 != null || Hour1 != null | Minute1 != null) 
            {
                if(Year1 != null && Month1 != null && Day1 != null && Hour1 != null && Minute1 != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            if (Year2 != null || Month2 != null || Day2 != null || Hour2 != null | Minute2 != null)
            {
                if (Year2 != null && Month2 != null && Day2 != null && Hour2 != null && Minute2 != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            return false;
        }
    }
}
