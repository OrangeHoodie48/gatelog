﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public interface TimeViewModel
    {
        DateTime DateAndTime { get; set; }
        string Day { get; set; }
        string Month { get; set; }
        string Year { get; set; }
        string Hour { get; set; }
        string Minute { get; set; }
    }
}
