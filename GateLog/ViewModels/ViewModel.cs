﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public abstract class ViewModel : TimeViewModel
    {

        public abstract string Door { get; set; }
        public abstract string Seal { get; set; }
        public abstract bool IsEmpty { get; set; }
        public abstract bool IsRelay { get; set; }
        public abstract bool InDropLot { get; set; }
        public abstract bool InYard { get; set; }
        public abstract bool OnFence { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string Hour { get; set; }
        public string Minute { get; set; }
        public DateTime DateAndTime { get; set; }


    }


}
