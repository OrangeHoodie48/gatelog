﻿using GateLog.Models;
using GateLog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class InLotViewModel
    {
        public List<InTractor> InTractors { get; set; }
        public List<InTrailer> InTrailers { get; set; }
        public List<Truck> Trucks { get; set; }
        public int EntryId { get; set; }
        public Dictionary<string, List<string>> CarrierMap { get; set; } 

        public void SeedTractors()
        {
            if(Trucks != null && InTractors != null)
            {
                foreach(InTractor t in InTractors)
                {
                    Truck tk = Trucks.FirstOrDefault(r => r.Id == t.EntryId);
                    t.Tractor = tk.Tractor;
                    t.Carrier = tk.Carrier;
                    t.DateAndTime = tk.DateAndTime; 
                }
            }
        }
        public void SeedTrailers()
        {
            if (Trucks != null && InTrailers != null)
            {
                foreach (InTrailer t in InTrailers)
                {
                    Truck tk = Trucks.FirstOrDefault(r => r.Id == t.EntryId);
                    t.Trailer = tk.Trailer;
                    t.Carrier = tk.Carrier;
                    t.DateAndTime = tk.DateAndTime;
                    t.LogId = tk.LogId;

                    if(tk.Trip != null)
                    {
                        if(!tk.ChangedEmptyStatus)
                        {
                            t.Status = StatusEnum.Inbound;
                        }
                        else
                        {
                            t.Status = StatusEnum.Empty;
                        }
                    }
                    else if (tk.IsEmpty)
                    {
                        if (!tk.ChangedEmptyStatus)
                        {
                            t.Status = StatusEnum.Empty;
                        }
                        else
                        {
                            t.Status = StatusEnum.Inbound;
                        }
                    }
                    else if (tk.IsRelay)
                    {
                        t.Status = StatusEnum.Relay; 
                    }
                    else if (tk.InDropLot)
                    {
                        t.Status = StatusEnum.Drop;
                    }
                    else
                    {
                        t.Status = StatusEnum.None; 
                    }

                }
            }
        }

        public void SeedCarrierMap(IDataServices dataServices)
        {
            CarrierMap = new Dictionary<string, List<string>>(); 
            List<string> relatedCarriers;
            List<string> s;
            foreach (string carrier1 in dataServices.GetAllCarrierNames())
            {
                if (dataServices.GetCarrierByName(carrier1).GetNames().Count > 1)
                {
                    relatedCarriers = dataServices.GetRelatedCarriersFromName(carrier1);
                    CarrierMap.Add(carrier1, relatedCarriers);
                }
                else
                {
                    s = new List<string>(); 
                    s.Add("No Related Carriers");
                    CarrierMap.Add(carrier1, s);
                }
            }
        }
    }
}
