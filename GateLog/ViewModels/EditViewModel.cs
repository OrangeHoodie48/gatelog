﻿using GateLog.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class EditViewModel : ViewModel
    {
        public int Id { get; set; }
        public List<Truck> Trucks = new List<Truck>();
        public string Trip { get; set; }
        public string Shipper { get; set; }
        [Required]
        public string Tractor { get; set; }
        [Required]
        public string Carrier { get; set; }
        public string Trailer { get; set; }
        public override string Seal { get; set; }
        [Required]
        public InOutEnum InOrOut { get; set; }
        public CompanyEnum Company { get; set; }
        public override bool IsEmpty { get; set; }
        public  bool RedirectedBecauseOfBadTrip { get; set; }
        public  bool RedirectedBecauseOfBadDoor { get; set; }
        public  bool RedirectedBecauseOfBadDate { get; set; }
        [MaxLength(3)]
        public override string Door { get; set; }
        public override bool InYard { get; set; }
        public override bool OnFence { get; set; }
        public override bool IsRelay { get; set; }
        public override bool InDropLot { get; set; }
        public new DateTime DateAndTime { get; set; }

        public string[] GetTrips()
        {
            string[] trips = Trip.Split(",");
            for (int i = 0; i < trips.Length; i++)
            {
                trips[i] = trips[i].Trim();
            }
            return trips;
        }

        public bool HasTrip(string trip)
        {
            if (Trip == null)
            {
                return false;
            }

            trip = trip.Trim();
            foreach (string s in GetTrips())
            {
                if (s.Trim().Equals(trip))
                {
                    return true;
                }
            }
            return false;
        }



    }
}
