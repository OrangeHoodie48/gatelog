﻿using GateLog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class TracTrailViewModel
    {
        public List<string> AllCarriers { get; set; }
        public string Trac { get; set; }
        public string Trail { get; set; }
        public string TracCarrier { get; set; }
        public string TrailCarrier { get; set; }
        public StatusEnum Status { get; set; }
        public bool TracIncomplete
        {
            get
            {
                if (Trac == null || TracCarrier == null)
                {
                    return true;
                }
                return false;
            }

        }
        public bool TrailIncomplete
        {
            get
            {
                if (Trail == null || TrailCarrier == null)
                {
                    return true;
                }
                return false;
            }

        }
    }
}
