﻿using GateLog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class LogViewModel
    {
        public List<LogEntry> LogEntries { get; set; }
    }
}
