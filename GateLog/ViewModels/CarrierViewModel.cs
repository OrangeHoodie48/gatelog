﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;

namespace GateLog.ViewModels
{
    public class CarrierViewModel
    {
        [Required]
        public string Name { get; set; }
        public bool CarrierAlreadyExists { get; set; }
        public List<string> CarrierNames { get; set; }
        public List<string> RelatedCarriers { get; set; }
        public string CarrierToRelate { get; set; }


    }
}
