﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Services;

namespace GateLog.ViewModels
{
    public class SearchViewModel
    {
        public bool WantsCSV { get; set; }
        public string Line { get; set; }
        public List<Truck> Trucks { get; set; }
        public int CurrentIndex { get; set; }
        public int NextIndex { get; set; }
        public int PreviousIndex { get; set; }
        public bool IsNextPage { get; set; }
        public bool IsFirstPage { get; set; }
        public Dictionary<string, List<string>> CarrierMap { get; set; }
        public void SeedCarrierMap(IDataServices dataServices)
        {
            CarrierMap = new Dictionary<string, List<string>>();
            List<string> relatedCarriers;
            List<string> s;
            foreach (string carrier1 in dataServices.GetAllCarrierNames())
            {
                if (dataServices.GetCarrierByName(carrier1).GetNames().Count > 1)
                {
                    relatedCarriers = dataServices.GetRelatedCarriersFromName(carrier1);
                    CarrierMap.Add(carrier1, relatedCarriers);
                }
                else
                {
                    s = new List<string>();
                    s.Add("No Related Carriers");
                    CarrierMap.Add(carrier1, s);
                }
            }
        }
    }
}
