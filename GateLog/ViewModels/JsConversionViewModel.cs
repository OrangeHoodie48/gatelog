﻿using GateLog.Models;
using GateLog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.ViewModels
{
    public class JsConversionViewModel
    {

        public Dictionary<string, List<string>> CarrierMap { get; set; }
        public List<string> FirstCarriers { get; set; }
        public List<InTrailer> InTrailers { get; set; }
        public List<InTractor> InTractors { get; set; }
        public Dictionary<string, List<InTrailer>> TrailerCarrierMap { get; set; }
        public Dictionary<string, List<InTractor>> TractorCarrierMap { get; set; }
        public Dictionary<string, string> FirstToRelatedCarrierMap { get; set; }

        public void SeedCarrierMap(IDataServices dataServices)
        {
            CarrierMap = new Dictionary<string, List<string>>();
            List<string> relatedCarriers;
            List<string> s;
            foreach (string carrier1 in dataServices.GetAllCarrierNames())
            {
                if (dataServices.GetCarrierByName(carrier1).GetNames().Count > 1)
                {
                    relatedCarriers = dataServices.GetRelatedCarriersFromName(carrier1);
                    CarrierMap.Add(carrier1, relatedCarriers);
                }
                else
                {
                    s = new List<string>();
                    s.Add("No Related Carriers");
                    CarrierMap.Add(carrier1, s);
                }
            }
        }
    }
}
