﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Services; 
namespace GateLog.ViewModels
{
    public class IndexViewModel : ViewModel
    {
        public string Shipper { get; set; }
        public List<Truck> Trucks = new List<Truck>();
        public string Trip { get; set; }
        [Required]
        public string Tractor { get; set; }
        [Required]
        public string Carrier { get; set; }
        public string Trailer { get; set; }
        public override string Seal { get; set; }
        [Required]
        public InOutEnum InOrOut { get; set; }
        public CompanyEnum Company { get; set; }
        public override bool IsEmpty { get; set; }
        public bool RedirectedBecauseOfBadTrip { get; set; }
        public bool RedirectedBecauseOfBadDoor { get; set; }
        [MaxLength(3)]
        public override string Door { get; set; }
        public override bool InYard { get; set; }
        public override bool OnFence { get; set; }
        public override bool IsRelay { get; set; }
        public override bool InDropLot { get; set; }
        public List<string> CarrierNames { get; set; }
        public List<InTractor> InTractors { get; set; }
        public List<Carrier> AllCarriers { get; set; }
        public Dictionary<string, List<string>> TractorsByCarrier { get; set; }
        public List<InTrailer> InTrailers { get; set; }
        public Dictionary<string, List<string>> TrailersByCarrier { get; set; }
        public bool WantsPrint { get; set; }
        
        public void SeedInTractors(IDataServices dataServices)
        {
            foreach(InTractor t in InTractors)
            {
                Truck tk = dataServices.GetById(t.EntryId);
                t.Carrier = tk.Carrier;
                t.Tractor = tk.Tractor; 
            }
        }

        public void SeedInTrailers(IDataServices dataServices)
        {
            foreach(InTrailer t in InTrailers)
            {
                Truck tk = dataServices.GetById(t.EntryId);
                t.Carrier = tk.Carrier;
                t.Trailer = tk.Trailer;
            }
        }

        public void FillTractorsByCarrier()
        {
            TractorsByCarrier = new Dictionary<string, List<string>>();
            foreach(Carrier c in AllCarriers)
            {
                foreach(string s in c.GetNames())
                {
                    TractorsByCarrier.Add(s, GetInTractorsForCarrier(s));
                }
            }
        }

        public void FillTrailersByCarrier()
        {
            TrailersByCarrier = new Dictionary<string, List<string>>();
            foreach (Carrier c in AllCarriers)
            {
                foreach (string s in c.GetNames())
                {
                    TrailersByCarrier.Add(s, GetInTrailersForCarrier(s));
                }
            }
        }

        public List<string> GetInTractorsForCarrier(string name)
        {
            List<string> inTractors = new List<string>(); 
            foreach(Carrier c in AllCarriers)
            {
                 
                if (c.HasCarrier(name))
                {
                   
                    foreach(InTractor t in InTractors)
                    {
                       

                        if (c.HasCarrier(t.Carrier))
                        {
                            inTractors.Add(t.Tractor); 
                        }
                    }
                    break;
                }
            }
            return inTractors;
        }

        public List<string> GetInTrailersForCarrier(string name)
        {
            List<string> inTrailers = new List<string>();
            foreach (Carrier c in AllCarriers)
            {

                if (c.HasCarrier(name))
                {

                    foreach (InTrailer t in InTrailers)
                    {


                        if (c.HasCarrier(t.Carrier))
                        {
                            inTrailers.Add(t.Trailer);
                        }
                    }
                    break;
                }
            }
            return inTrailers;
        }
        public string[] GetTrips()
        {
            string[] trips = Trip.Split(",");
            for (int i = 0; i < trips.Length; i++)
            {
                trips[i] = trips[i].Trim();
            }
            return trips;
        }

        public bool HasTrip(string trip)
        {
            if (Trip == null)
            {
                return false;
            }

            trip = trip.Trim();
            foreach (string s in GetTrips())
            {
                if (s.Trim().Equals(trip))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
