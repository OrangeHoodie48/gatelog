﻿using GateLog.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.ViewModels; 

namespace GateLog.ViewComponents
{
    public class JsInTrailersViewComponent : ViewComponent
    {
        IDataServices dataServices;
        public JsInTrailersViewComponent(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }
        
        public IViewComponentResult Invoke()
        {
            JsConversionViewModel viewModel = new JsConversionViewModel();
            viewModel.InTrailers = dataServices.GetAllInTrailers();
            return View(viewModel);
        }
    }
}
