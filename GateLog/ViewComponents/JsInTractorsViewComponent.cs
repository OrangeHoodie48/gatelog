﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Services;
using GateLog.ViewModels; 
namespace GateLog.ViewComponents
{
    public class JsInTractorsViewComponent : ViewComponent
    {
        IDataServices dataServices;
        public JsInTractorsViewComponent(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IViewComponentResult Invoke()
        {
            JsConversionViewModel viewModel = new JsConversionViewModel();
            viewModel.InTractors = dataServices.GetAllInTractors();
            return View(viewModel);
        }
    }
}
