﻿using GateLog.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.ViewModels;

namespace GateLog.ViewComponents
{
    public class JsTrailerTractorCarrierMapsViewComponent : ViewComponent
    {
        IDataServices dataServices;
        public JsTrailerTractorCarrierMapsViewComponent(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }

        public IViewComponentResult Invoke()
        {
            JsConversionViewModel viewModel = new JsConversionViewModel();
            viewModel.FirstCarriers = dataServices.GetFirstCarriers(); 
            viewModel.FirstToRelatedCarrierMap = dataServices.GetRelatedToFirstCarrierMap();
            viewModel.TractorCarrierMap = dataServices.GetCarrierTractorMap();
            viewModel.TrailerCarrierMap = dataServices.GetCarrierTrailerMap();
            return View(viewModel);
        }
    }
}
