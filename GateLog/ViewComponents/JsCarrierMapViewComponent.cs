﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Services;
using GateLog.ViewModels;
namespace GateLog.ViewComponents
{
    public class JsCarrierMapViewComponent : ViewComponent
    {
        IDataServices dataServices; 
        public JsCarrierMapViewComponent(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }


        public IViewComponentResult Invoke()
        {
            JsConversionViewModel viewModel = new JsConversionViewModel();
            viewModel.SeedCarrierMap(dataServices);
            return View(viewModel); 
        }
    }
}
