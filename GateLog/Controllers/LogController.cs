﻿using GateLog.Models;
using GateLog.Services;
using GateLog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Controllers
{
    [Authorize]
    public class LogController : Controller
    {
        IDataServices dataServices; 
        public LogController(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }

        public IActionResult LogPage()
        {
            LogViewModel viewModel = new LogViewModel();
            viewModel.LogEntries = dataServices.GetAllLogEntries();
            if(viewModel.LogEntries.Count > 500)
            {
                viewModel.LogEntries = viewModel.LogEntries.GetRange(0, 500);
            }
            return View(viewModel);
        }

        public IActionResult SpecificLog(int id)
        {
            LogEntry log = dataServices.GetAllLogEntries().FirstOrDefault(r => r.Id == id);
            return View(log);
        }
    }
}
