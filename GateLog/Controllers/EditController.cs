﻿using GateLog.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers 
{
    [Authorize]
    public class EditController : Controller
    {
        IDataServices dataServices;
        IDateTimeService dateTimeServices;
        public EditController(IDataServices dataServices, IDateTimeService dateTimeServices)
        {
            this.dataServices = dataServices;
            this.dateTimeServices = dateTimeServices;
        }

        [HttpGet]
        public IActionResult EditTruck(int id)
        {
            Truck truck = dataServices.GetById(id);
            EditViewModel model = new EditViewModel();

            model.Carrier = truck.Carrier;
            model.DateAndTime = truck.DateAndTime;
            model.Door = truck.Door;
            model.Id = truck.Id;
            model.Seal = truck.Seal;
            model.Door = truck.Door;
            model.Tractor = truck.Tractor;
            model.Trailer = truck.Trailer;
            model.Trip = truck.Trip;
            model.InYard = truck.InYard;
            model.IsEmpty = truck.IsEmpty;
            model.IsRelay = truck.IsRelay; 
            model.InDropLot = truck.InDropLot;
            model.OnFence = truck.OnFence;
            model.InOrOut = truck.InOrOut;
            model.Company = truck.Company;
            model.Shipper = truck.Shipper;


            return View(model);

        }

        [HttpPost]
        public IActionResult EditTruck(EditViewModel editModel)
        {

            editModel.RedirectedBecauseOfBadTrip = false;
            editModel.RedirectedBecauseOfBadDoor = false;
            editModel.RedirectedBecauseOfBadDate = false;

            if (!ModelState.IsValid)
            {
                editModel.Trucks = dataServices.GetLastNTrucksFromList(20, dataServices.GetAllTrucks());
                return View(editModel);
            }
            Truck model = dataServices.GetById(editModel.Id);

            if ((editModel.Trip != null && dataServices.AreValidTrips(editModel.GetTrips()))
                || editModel.Trip == null)
            {
                model.Trip = editModel.Trip;
            }

            else
            {
                editModel.RedirectedBecauseOfBadTrip = true;
                editModel.Trucks = dataServices.GetLastNTrucksFromList(20, dataServices.GetAllTrucks());
                return View(editModel);
            }


            if (!dataServices.FilterDoor(editModel))
            {
                editModel.RedirectedBecauseOfBadDoor = true;
                editModel.Trucks = dataServices.GetLastNTrucksFromList(20, dataServices.GetAllTrucks());
                return View(editModel);
            }

            if(!dateTimeServices.ConvertDateTime(editModel, model))
            {
                editModel.RedirectedBecauseOfBadDate = true;
                editModel.Trucks = dataServices.GetLastNTrucksFromList(20, dataServices.GetAllTrucks());
                return View(editModel);
            }

            dataServices.FilterSeal(editModel);

            if(model.LogId != null)
            {
                if(editModel.Trailer == null)
                {
                    model.LogId = null; 
                }
                else if(model.Trailer != editModel.Trailer)
                {
                    model.LogId = null;
                }
            }
            if(model.Trailer != null && editModel.Trailer == null)
            {
                editModel.InDropLot = false;
                editModel.InYard = false;
                editModel.IsRelay = false;
                editModel.OnFence = false;
                editModel.IsEmpty = false;
                editModel.Door = null;

                
            }
            


            model.Carrier = editModel.Carrier;
            model.Shipper = editModel.Shipper;
            model.Tractor = editModel.Tractor;
            model.InOrOut = editModel.InOrOut;
            model.Seal = editModel.Seal;
            model.IsEmpty = editModel.IsEmpty;
            model.InDropLot = editModel.InDropLot;
            model.IsRelay = editModel.IsRelay;
            model.InYard = editModel.InYard;
            model.OnFence = editModel.OnFence;
            model.Trailer = editModel.Trailer;
            model.Door = editModel.Door;
            model.Id = editModel.Id;
            model.Company = editModel.Company;
            dataServices.UpdateTruck(model);

            if (dataServices.GetInTrailer(model.Id) != null)
            {
                if (editModel.Trailer == null || editModel.InOrOut == InOutEnum.Out)
                {
                    dataServices.RemoveInTrailer(editModel.Id);
                }
            }
            else
            {
                if (editModel.Trailer != null && editModel.InOrOut == InOutEnum.In)
                {
                    dataServices.AddInTrailer(model);
                }
            }

            return RedirectToAction("HistoryPage", "History");


        }

        public IActionResult RemoveTruck(int id)
        {
            dataServices.RemoveTruck(id);
            if(dataServices.GetInTractor(id) != null)
            {
                dataServices.RemoveInTractorFromEntryId(id);
            }
            if(dataServices.GetInTrailer(id) != null)
            {
                dataServices.RemoveInTrailerFromEntryId(id);
            }
            return RedirectToAction("Index", "Home");
        }

    }
}
