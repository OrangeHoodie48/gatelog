﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Services;
using GateLog.Models;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers
{
    [Authorize]
    public class PrintController : Controller
    {
        IDataServices dataServices; 
        public PrintController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        
        public IActionResult PrintPage(int id)
        {
            Truck truck = dataServices.GetById(id);
            return View(truck);
        }
        public IActionResult PrintPageFromHome(Truck t)
        {   
            return View("PrintPage", t);
        }


    }
}
