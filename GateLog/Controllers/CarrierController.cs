﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Services;
using GateLog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace GateLog.Controllers
{
    [Authorize]
    public class CarrierController : Controller
    {
        IDataServices dataServices;
        public CarrierController(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }

        public IActionResult Carriers()
        {
            return View();
        }


        [HttpGet]
        public IActionResult AddCarrier()
        {
       
            CarrierViewModel model = new CarrierViewModel();
            return View(model);
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddCarrier(CarrierViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(viewModel);
            }
            Carrier carrier = new Carrier();
            carrier.AddCarrierName(viewModel.Name);

            if (dataServices.AddCarrier(carrier) == null)
            {
                viewModel.CarrierAlreadyExists = true;
                return View(viewModel);
            }

            return RedirectToAction("AllCarriers", "Carrier");
        }

        public IActionResult AllCarriers()
        {
            CarrierViewModel viewModel = new CarrierViewModel();
            viewModel.CarrierNames = dataServices.GetAllCarrierNames();
            return View(viewModel); 
        }




        public IActionResult CarrierDetails(string name)
        {
            CarrierViewModel viewModel = new CarrierViewModel();
            viewModel.Name = name;
            List<string> relatedCarriers = dataServices.GetRelatedCarriersFromName(name);
            viewModel.RelatedCarriers = relatedCarriers;
            Carrier c = dataServices.GetCarrierByName(name);
            viewModel.CarrierNames = dataServices.GetPotentialRelatableCarriers(c);
            for(int i = 0; i < viewModel.RelatedCarriers.Count; i++)
            {
                viewModel.RelatedCarriers[i] = viewModel.RelatedCarriers[i] + " ";
            }
            return View(viewModel);
        }

        [ValidateAntiForgeryToken]
        public IActionResult RelateCarrier(CarrierViewModel viewModel)
        {
            Carrier car1 = dataServices.GetCarrierByName(viewModel.Name);
            Carrier car2 = dataServices.GetCarrierByName(viewModel.CarrierToRelate);
            dataServices.RelateCarriers(car1, car2);
            return RedirectToAction("AllCarriers", "Carrier");
        }

        [HttpPost]
        public IActionResult DeleteCarrier(string name)
        {
            Carrier c = dataServices.GetCarrierByName(name);
            if (c.GetNames().Count == 1)
            {
                dataServices.RemoveCarrier(c);
            }
            else
            {
                dataServices.RemoveCarrierName(name);
                //dataServices.RemoveCarrier(c);
            }
            return RedirectToAction("AllCarriers", "Carrier");
        }
    }
}
