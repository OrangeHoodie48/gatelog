﻿using GateLog.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.ViewModels;
using GateLog.Models;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers
{
    [Authorize]
    public class TracTrailController : Controller
    {
        IDataServices dataServices; 
        public TracTrailController(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }

        [HttpGet]
        public IActionResult Add()
        {
            if (!User.Identity.Name.Equals("Admin2018")) return RedirectToAction("Index", "Home");

            TracTrailViewModel viewModel = new TracTrailViewModel();
            viewModel.AllCarriers = dataServices.GetAllCarrierNames(); 
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Add(TracTrailViewModel viewModel)
        {
            if (!User.Identity.Name.Equals("Admin2018")) return RedirectToAction("Index", "Home");
            if (viewModel.TracIncomplete)
            {
                if (viewModel.TrailIncomplete)
                {
                    ModelState.AddModelError("", "Must complete either all information for either a tractor or trailer");
                    return View(viewModel);
                }
            }
            else if (!viewModel.TracIncomplete)
            {
                InTractor trac = new InTractor();
                trac.Carrier = viewModel.TracCarrier;
                trac.Tractor = viewModel.Trac;
                dataServices.AddInTractor(trac);
                return RedirectToAction("Add");
            }

            InTrailer trail = new InTrailer();
            trail.Carrier = viewModel.TrailCarrier;
            trail.Trailer = viewModel.Trail;
            if (viewModel.Status == StatusEnum.None)
            {
                trail.Status = StatusEnum.None; 
            }
            else if (viewModel.Status == StatusEnum.Empty )
            {
                trail.Status = StatusEnum.Empty; 
            }
            else if (viewModel.Status == StatusEnum.Drop)
            {
                trail.Status = StatusEnum.Drop; 
            }
            else if (viewModel.Status == StatusEnum.Relay)
            {
                trail.Status = StatusEnum.Relay; 
            }
            else
            {
                trail.Status = StatusEnum.Inbound; 
            }
            dataServices.AddInTrailer(trail);
            return RedirectToAction("Add");
        }
    }
}
