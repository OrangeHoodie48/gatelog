﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Services;
using GateLog.ViewModels;
using GateLog.Models;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers
{
    [Authorize]
    public class InLotController : Controller
    {
        IDataServices dataServices;
        public InLotController(IDataServices dataServices)
        {
            this.dataServices = dataServices;
        }

        public IActionResult TractorsInLot()
        {
            InLotViewModel viewModel = new InLotViewModel();
            viewModel.Trucks = dataServices.GetAllTrucks();
            viewModel.InTractors = dataServices.GetAllInTractors();
            viewModel.SeedCarrierMap(dataServices);
            return View(viewModel);
        }

        public IActionResult RemoveInTractor(int id)
        {
            dataServices.RemoveInTractor(id);
            return RedirectToAction("TractorsInLot", "InLot");
        }

        public IActionResult ToggleFilledStatus(int id)
        {
            dataServices.ToggleFilledStatus(id);
            return RedirectToAction("TrailersInLot");
        }

        public IActionResult TrailersInLot()
        {
            InLotViewModel viewModel = new InLotViewModel();
            viewModel.Trucks = dataServices.GetAllTrucks();
            viewModel.InTrailers = dataServices.GetAllInTrailers();
            viewModel.SeedCarrierMap(dataServices);
            return View(viewModel);
        }

        public IActionResult RemoveInTrailer(int id)
        {
            dataServices.RemoveInTrailer(id);
            return RedirectToAction("TrailersInLot", "InLot");
        }
    }
}
