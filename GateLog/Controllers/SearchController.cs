﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Services;
using GateLog.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;

namespace GateLog.Controllers
{
    [Authorize]
    public class SearchController : Controller
    {
        IDataServices dataServices;
        IDateTimeService dateTimeService;
        public SearchController(IDataServices dataServices, IDateTimeService dateTimeService)
        {
            this.dataServices = dataServices;
            this.dateTimeService = dateTimeService;
        }

        public IActionResult SearchResults(string trip)
        {
            if (!dataServices.AreValidTrips(trip.Split(",")))
            {
                CriteriaViewModel viewModel = new CriteriaViewModel();
                viewModel.TripError = true;
                return View("SearchPage", viewModel);
            }
            else if (dataServices.GetByTrip(trip).Count == 0)
            {
                CriteriaViewModel viewModel = new CriteriaViewModel();
                viewModel.TripError = true;
                return View("SearchPage", viewModel);
            }

            SearchViewModel model = new SearchViewModel();
            model.SeedCarrierMap(dataServices);
            model.Trucks = dataServices.GetByTrip(trip);
            return View(model);
        }


        [HttpGet]
        public IActionResult SearchPage()
        {
            CriteriaViewModel model = new CriteriaViewModel();
            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public IActionResult SearchPage(CriteriaViewModel viewModel)
        {
            if (viewModel.Carrier != null)
            {
                viewModel.Carrier = viewModel.Carrier.ToUpper();
            }
            if (viewModel.TimeIsIncomplete())
            {
                ModelState.AddModelError("", "Must fill all or none of the time fields out.");
                return View(viewModel);
            }

            if (viewModel.Carrier != null && !dataServices.GetAllCarrierNames().Contains(viewModel.Carrier))
            {
                ModelState.AddModelError("", "No Such Carrier Exists.");
                return View(viewModel);
            }

            if (!viewModel.IsNotEmpty())
            {
                viewModel.NoFieldError = true;
                return View(viewModel);
            }

            CriteriaChart criteria = new CriteriaChart();

            if (!dateTimeService.ConvertDateTime(viewModel))
            {
                viewModel.DateOrTimeError = true;

                if (viewModel.TimeIsIncomplete())
                {
                    ModelState.AddModelError("", "Must fill all or none of the time fields out.");
                }

                return View(viewModel);
            }

            if (viewModel.From > viewModel.To)
            {
                viewModel.ToIsBeforeFrom = true;
                return View(viewModel);
            }
            viewModel.LoadCriteriaChart(criteria);

            criteria.ActualCarrierObject = dataServices.GetCarrierByName(criteria.Carrier);

            List<Truck> trucks = dataServices.GetAllByCrieteriaChart(criteria);
            SearchViewModel searchModel = new SearchViewModel();
            searchModel.Trucks = trucks;
            if (searchModel.Trucks.Count == 0)
            {
                viewModel.NoResults = true;
                return View(viewModel);
            }
            searchModel.SeedCarrierMap(dataServices);

            if (viewModel.WantsCSV)
            {
                string line = "Date, Time, Carrier, Tractor, Trailer, Trip, Seal/Status, In/Out, Door, Company\n";
                foreach (Truck t in searchModel.Trucks)
                {
                    string[] DateAndTime = t.DateAndTime.ToString().Split(" ");
                    line += DateAndTime[0] + ",";
                    line += DateAndTime[1] + ",";
                    line += t.Carrier + ",";
                    line += t.Tractor + ",";
                    line += t.Trailer + ",";
                    line += t.Trip + ",";
                    if (t.IsEmpty)
                    {
                        line += "Empty" + ",";
                    }
                    else if (t.IsRelay)
                    {
                        line += "Relay" + ",";
                    }
                    else if (t.InDropLot)
                    {
                        line += "Drop Lot" + ",";
                    }
                    else
                    {
                        line += t.Seal + ",";
                    }

                    if (t.InOrOut == InOutEnum.In)
                    {
                        line += "In" + ",";
                    }
                    else
                    {
                        line += "Out" + ",";
                    }

                    line += t.Door + ",";

                    if (t.Company == CompanyEnum.ESSENDANT)
                    {
                        line += "ESSENDANT" + ",";
                    }
                    else if (t.Company == CompanyEnum.NCS)
                    {
                        line += "NCS" + ",";
                    }
                    else
                    {
                        line += "OTHER";
                    }
                    line += "\n";
                }
                viewModel.Line = line;
                viewModel.WantsCSV = true;
                return View("SearchPage", viewModel); 
            }

            return View("SearchResults", searchModel);
        }
    }
}
