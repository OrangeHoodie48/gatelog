﻿using GateLog.Models;
using GateLog.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Controllers
{
    public class AccountController : Controller
    {
        UserManager<IdentityModel> userManager;
        SignInManager<IdentityModel> signInManager;
        public AccountController(SignInManager<IdentityModel> signInManager, UserManager<IdentityModel> userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        [HttpGet]
        public async Task<IActionResult> Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            

            var result = await userManager.FindByNameAsync("Security2018");
            if(result == null)
            {
                IdentityModel user = new IdentityModel();
                user.UserName = "Security2018";
                await userManager.CreateAsync(user, "Security@NCS101!"); 
            }
            result = await userManager.FindByNameAsync("Admin2018");
            if (result == null)
            {
                IdentityModel user2 = new IdentityModel();
                user2.UserName = "Admin2018";
                await userManager.CreateAsync(user2, "Admin@NCS101!");
            }

            LoginViewModel model = new LoginViewModel();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid && (model.UserName != null || model.Password != null))
            {

                var result = await signInManager.PasswordSignInAsync(model.UserName, model.Password, true, false);
                if (result.Succeeded)
                {
                    if (Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        return Redirect(Request.Query["ReturnUrl"].First());
                    }
                    return RedirectToAction("Index", "Home");
                }
                
            }
            ModelState.AddModelError("", "Invalid Username or Password");
            return View();
        }

        [Authorize]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return View("Login");
        }
    }

}
