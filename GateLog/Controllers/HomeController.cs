﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.ViewModels;
using GateLog.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        UserManager<IdentityModel> userManager;
        IDataServices dataServices; 
        public HomeController(IDataServices dataServices, UserManager<IdentityModel> userManager)
        {
            this.dataServices = dataServices;
            this.userManager = userManager; 
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {

            IndexViewModel model = new IndexViewModel();
            model.CarrierNames = dataServices.GetAllCarrierNames();
            model.InTractors = dataServices.GetAllInTractors();
            model.InTrailers = dataServices.GetAllInTrailers();
            model.AllCarriers = dataServices.GetAllCarriers();
            if (model.AllCarriers.Count > 0)
            {
                model.Carrier = model.AllCarriers[0].GetNames()[0];
                model.FillTractorsByCarrier();
                model.FillTrailersByCarrier();
            }

            IdentityModel user = await userManager.GetUserAsync(HttpContext.User);
            if (user.WantsView1)
            {
                return View(model);
            }

            return View("Index2", model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(IndexViewModel inputModel)
        {
            inputModel.RedirectedBecauseOfBadTrip = false;
            inputModel.RedirectedBecauseOfBadDoor = false;
            
            if (!ModelState.IsValid)
            {
                inputModel.CarrierNames = dataServices.GetAllCarrierNames();
                
                inputModel.InTractors = dataServices.GetAllInTractors();
                inputModel.InTrailers = dataServices.GetAllInTrailers();
                inputModel.AllCarriers = dataServices.GetAllCarriers();
                inputModel.Carrier = inputModel.AllCarriers[0].GetNames()[0];
                inputModel.FillTractorsByCarrier();
                inputModel.FillTrailersByCarrier();
                return View(inputModel);
            }
            Truck model = new Truck();

            if ((inputModel.Trip != null && dataServices.AreValidTrips(inputModel.GetTrips()) && dataServices.AreNonDuplicateTrips(inputModel.GetTrips())) 
                || inputModel.Trip == null)
            {
                model.Trip = inputModel.Trip;
            }

            else
            {
                inputModel.RedirectedBecauseOfBadTrip = true;
                inputModel.CarrierNames = dataServices.GetAllCarrierNames();
                inputModel.InTractors = dataServices.GetAllInTractors();
                inputModel.InTrailers = dataServices.GetAllInTrailers();
                inputModel.AllCarriers = dataServices.GetAllCarriers();
                inputModel.Carrier = inputModel.AllCarriers[0].GetNames()[0];
                inputModel.FillTractorsByCarrier();
                inputModel.FillTrailersByCarrier();
                return View(inputModel);
            }


            if (!dataServices.FilterDoor(inputModel))
            {
                inputModel.RedirectedBecauseOfBadDoor = true;
                inputModel.CarrierNames = dataServices.GetAllCarrierNames();
                inputModel.InTractors = dataServices.GetAllInTractors();
                inputModel.InTrailers = dataServices.GetAllInTrailers();
                inputModel.AllCarriers = dataServices.GetAllCarriers();
                inputModel.Carrier = inputModel.AllCarriers[0].GetNames()[0];
                inputModel.FillTractorsByCarrier();
                inputModel.FillTrailersByCarrier();
                return View(inputModel);
            }

            dataServices.FilterSeal(inputModel);
            

            model.Carrier = inputModel.Carrier.ToUpper();
            model.Tractor = inputModel.Tractor.Trim();
            model.InOrOut = inputModel.InOrOut;
            model.Seal = inputModel.Seal;
            model.IsEmpty = inputModel.IsEmpty;
            model.InDropLot = inputModel.InDropLot;
            model.IsRelay = inputModel.IsRelay; 
            model.InYard = inputModel.InYard;
            model.Shipper = inputModel.Shipper;
            model.OnFence = inputModel.OnFence; 
            if(inputModel.Trailer != null) {
                model.Trailer = inputModel.Trailer.Trim();
            }
            else
            {
                model.Trailer = inputModel.Trailer;
            }
            if (inputModel.Door != null)
            {

                model.Door = inputModel.Door.Trim();
            }
            else
            {
                model.Door = inputModel.Door;
            }
            model.Company = inputModel.Company;
            model.DateAndTime = DateTime.Now; 
            dataServices.AddTruck(model);

            if(inputModel.WantsPrint == true)
            {
                return RedirectToAction("PrintPageFromHome", "Print", model);
            }

            return RedirectToAction("Index");
            

        }

        public async Task<IActionResult> SwitchView()
        {
            IdentityModel user = await userManager.GetUserAsync(HttpContext.User);
            if (user.WantsView1)
            {
                user.WantsView1 = false;
            }
            else
            {
                user.WantsView1 = true;
            }
            await userManager.UpdateAsync(user);

            return RedirectToAction("Index");
        }



    }
}
