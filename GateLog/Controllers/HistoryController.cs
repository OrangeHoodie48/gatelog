﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Services;
using GateLog.Models;
using GateLog.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace GateLog.Controllers
{
    [Authorize]
    public class HistoryController : Controller
    {
        IDataServices dataServices;
        public HistoryController(IDataServices dataServices)
        {
            this.dataServices = dataServices; 
        }

        public IActionResult HistoryPage(int i)
        {
            List<Truck> trucks = dataServices.Get200From(i);
            if(trucks.Count == 0)
            {
                return View("NoHistory");
            }
            SearchViewModel model = new SearchViewModel();
            model.Trucks = trucks;
            model.CurrentIndex = i;
            if((dataServices.GetAllTrucks().Count -1) > i + 200)
            {
                model.IsNextPage = true;
            }
            else
            {
                model.IsNextPage = false;
            }
            if(i == 0)
            {
                model.IsFirstPage = true;
            }
            model.SeedCarrierMap(dataServices);
            return View(model);

        }
    }
}
