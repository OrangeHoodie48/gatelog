﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public enum StatusEnum
    {
        Inbound, Relay, Drop, Empty, None
    }
}
