﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class InTractor
    {
        public int Id { get; set; }
        public int EntryId { get; set; }
        public string Carrier { get; set; }
        public string Tractor { get; set; }
        public DateTime DateAndTime { get; set; }

    }
}
