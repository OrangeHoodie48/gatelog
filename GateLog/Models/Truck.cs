﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace GateLog.Models
{

    public class Truck
    {
        public int Id { get; set; }

        public string Shipper { get; set; }
        public string Trip { get; set; }

        [Required]
        public string Tractor { get; set; }
        [Required]
        public string Carrier { get; set; }
        public string Trailer { get; set; }
        public string Seal { get; set; }
        public DateTime DateAndTime { get; set; }
        [Required]
        public InOutEnum InOrOut { get; set; }
        public bool IsEmpty { get; set; }
        [MaxLength(3)]
        public string Door { get; set; }
        public bool InYard { get; set; }
        public bool OnFence { get; set; }
        public bool IsRelay { get; set; }
        public bool InDropLot { get; set; }
        public string LogId { get; set; }
        public CompanyEnum Company {get; set; }
        public bool ChangedEmptyStatus { get; set; }

        public string[] GetTrips()
        {
            string[] trips = Trip.Split(","); 
            for(int i = 0; i < trips.Length; i++)
            {
                trips[i] = trips[i].Trim();
            }
            return trips;
        }

        public bool HasTrip(string trip)
        {
            if (Trip == null)
            {
                return false;
            }

            trip = trip.Trim();
            foreach(string s in GetTrips())
            {
                if (s.Trim().Equals(trip))
                {
                    return true;
                }
            }
            return false;
        }

    }

}
