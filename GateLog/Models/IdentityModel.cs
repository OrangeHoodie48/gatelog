﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class IdentityModel : IdentityUser
    {
        public bool WantsView1 { get; set; }
    }
}
