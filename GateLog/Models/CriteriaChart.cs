﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class CriteriaChart
    {
        
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Tractor { get; set; }
        public string Carrier { get; set; }
        public string Trailer { get; set; }
        public string Trip { get; set; }
        public SearchPageCompanyEnum Company { get; set; }
        public Carrier ActualCarrierObject { get; set; }
       

    }
}
