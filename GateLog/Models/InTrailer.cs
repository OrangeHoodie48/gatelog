﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class InTrailer
    {
        public int Id { get; set; }
        public int EntryId { get; set; }
        public string Carrier { get; set; }
        public string Trailer { get; set; }
        public DateTime DateAndTime { get; set; }
        public string LogId { get; set; }
        public StatusEnum Status { get; set; }
        public bool ChangedEmptyStatus { get; set; }

    }
}
