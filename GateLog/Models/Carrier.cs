﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class Carrier
    {
        public int Id { get; set; }
        public string Names { get; set; }

        public bool HasCarrier(string name)
        {
            //added 4/12/2018 
            if (name== null || name.Equals("")) return false;

            name = name.Trim().ToUpper();
            bool result = false;
            foreach (string n in GetNames())
            {
                if (n.Equals(name))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        public bool AreRelatedCarrierNames(string name1, string name2)
        {
            name1 = name1.Trim().ToUpper();
            name2 = name2.Trim().ToUpper();
            if (GetNames().FirstOrDefault(r => r.Equals(name1)) != null)
            {
                if (GetNames().FirstOrDefault(r => r.Equals(name2)) != null)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddCarrierName(string name)
        {
            name = name.Trim().ToUpper();
            if (Names == null)
            {
                Names = name;
            }
            else
            {
                Names += "%DELIMETER%" + name;
            }
        }

        public List<string> GetNames()
        {
            List<string> names = Names.Split("%DELIMETER%").ToList();
            return names; 
        }

        public void RemoveCarrierName(string name)
        {
            string tempNames = "";
            name = name.ToUpper().Trim();
            List<string> names = GetNames();
            if (names.Count == 0)
            {
                if (Names == null)
                {
                    return;
                }
                else
                {
                    if (Names.Equals(name))
                    {
                        Names = null;
                        return;
                    }
                }
            }
            else
            {
                foreach (string n in names)
                {
                    if (!n.Equals(name))
                    {
                        tempNames += "%DELIMETER%" + n;
                    }
                }
                Names = tempNames;
            }
            if (Names.StartsWith("%DELIMETER%"))
            {
                Names = Names.Substring(11);
            }
            Names = Names.Replace("%DELIMETER%%DELIMETER%", "%DELIMETER%");
        }

    }
}
