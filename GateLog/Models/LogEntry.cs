﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Models
{
    public class LogEntry
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public DateTime DateAndTIme { get; set; }
        public bool Flag { get; set; }
    }
}
