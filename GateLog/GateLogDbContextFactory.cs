﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace GateLog
{
    public class GateLogDbContextFactory : IDesignTimeDbContextFactory<GateLogDbContext>
    {
        public GateLogDbContextFactory()
        {

        }

        IConfigurationRoot config = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

        public GateLogDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<GateLogDbContext>();
            optionsBuilder.UseSqlServer(config.GetConnectionString("GateLog"));

            return new GateLogDbContext(optionsBuilder.Options);
        }
    }
}
