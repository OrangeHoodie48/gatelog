﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Delegates.Tools
{
    public class BoolBox
    {
        public bool TruthValue { get; set; }
    }
}
