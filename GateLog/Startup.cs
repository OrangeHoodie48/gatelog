﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GateLog.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Routing;
using GateLog.Services;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Rewrite;
using GateLog.Models;
using Microsoft.AspNetCore.Identity;

namespace GateLog
{
    public class Startup
    {
        IConfiguration configuration; 
        public Startup(IConfiguration configuration)
        {
            this.configuration = configuration;

        }
       

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<GateLogDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("GateLog")));
            services.AddScoped<IDataServices, DataService>();
            services.AddSingleton<IFilterMachine, FilterMachine>();
            services.AddScoped<IDateTimeService, DateTimeService>();
            services.AddIdentity<IdentityModel, IdentityRole>().AddEntityFrameworkStores<GateLogDbContext>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment() || env.IsProduction())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRewriter(new RewriteOptions().AddRedirectToHttpsPermanent());
            app.UseStaticFiles();
            // app.UseNodeModules(env.ContentRootPath);

            app.UseAuthentication(); 
            app.UseMvc(ConfigureRoutes);

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }

        void ConfigureRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Default", "{controller=Home}/{action=Index}/{id?}");
        }

    }
}
