﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace GateLog.Data
{
    public class GateLogDbContext : IdentityDbContext<IdentityModel> 
    {
        public GateLogDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Truck> Trucks { get; set; }
        public DbSet<Carrier> Carriers { get; set; }
        public DbSet<InTractor> InTractors { get; set; }
        public DbSet<InTrailer> InTrailers { get; set; }
        public DbSet<LogEntry> LogEntries { get; set; }


    }
}
