﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Data;
using GateLog.ViewModels;
using GateLog.Models;

namespace GateLog.Services
{
    public interface IDataServices
    {
        Truck AddTruck(Truck truck);
        List<Truck> GetAllTrucks();
        List<Truck> GetLastNTrucksFromList(int n, List<Truck> trucks);
        Truck UpdateTruck(Truck truck);
        List<Truck> GetByTrip(string trip);
        Truck GetById(int id);
        List<Truck> GetAllByCrieteriaChart(CriteriaChart criteria);
        bool AreValidTrips(string[] trips);
        void FilterSeal(ViewModel inputModel);
        bool FilterDoor(ViewModel inputModel);
        bool AreNonDuplicateTrips(string[] trips);
        Carrier GetCarrierById(int id);
        Carrier GetCarrierByName(string name);
        Carrier RelateCarriers(Carrier car1, Carrier car2);
        List<Carrier> GetAllCarriers();
        List<string> GetAllCarrierNames();
        bool AreSameCarrier(string name1, string name2);
        List<string> GetAllNamesForCarrier(Carrier c);
        Carrier RemoveCarrier(Carrier c);
        void RemoveCarrierName(string name);
        Carrier AddCarrier(Carrier carrier);
        List<string> GetRelatedCarriersFromName(string name);
        List<string> GetPotentialRelatableCarriers(Carrier c);
        InTractor GetInTractor(int entryId);
        InTrailer GetInTrailer(int entryId);
        InTractor AddInTractor(Truck t);
        InTrailer AddInTrailer(Truck t);
        InTractor RemoveInTractor(int entryId);
        InTrailer RemoveInTrailer(int entryId);
        List<InTractor> GetAllInTractors();
        List<InTrailer> GetAllInTrailers();
        LogEntry AddLogEntry(LogEntry log);
        List<LogEntry> GetAllLogEntries();
        InTrailer UpdateInTrailer(InTrailer inTrailer);
        List<Truck> Get200From(int i);
        List<string> GetAllInTractorNumbersOfCarrier(string carrierName);
        InTrailer ToggleFilledStatus(int iD);
        List<string> GetFirstCarriers();
        Dictionary<string, string> GetRelatedToFirstCarrierMap();
        Dictionary<string, List<InTrailer>> GetCarrierTrailerMap();
        Dictionary<string, List<InTractor>> GetCarrierTractorMap();
        InTrailer AddInTrailer(InTrailer t);
        InTractor AddInTractor(InTractor t);
        void RemoveTruck(int id);
        void RemoveLogEntry(int id);
        InTractor RemoveInTractorFromEntryId(int entryId);
        InTrailer RemoveInTrailerFromEntryId(int entryId);
    }
}
