﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Services
{
    public class DateTimeBox
    {
        public DateTimeBox(int day, int year, int hour, int month, int minute)
        {
            Day = day;
            Year = year;
            Hour = hour;
            Month = month;
            Minute = minute;
        }

        public int Day { get; set; }
        public int Year { get; set; }
        public int Hour { get; set; }
        public int Month { get; set; }
        public int Minute { get; set; }

        public DateTime ConvertToDateTime()
        {
            return new DateTime(Year, Month, Day, Hour, Minute, 0);
        }

    }
}
