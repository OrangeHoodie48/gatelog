﻿using GateLog.Models;
using GateLog.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Services
{
    public class DateTimeService : IDateTimeService
    {

        public bool ConvertDateTime(TimeViewModel viewModel)
        {
            if (viewModel is CriteriaViewModel && viewModel.Day == null)
            {
                ((CriteriaViewModel)(viewModel)).CheckAndResolveEmptyTimeFields();
                ((CriteriaViewModel)(viewModel)).PrimeFromForConversion();
                if (((CriteriaViewModel)(viewModel)).FromDoesNotEqualsTo())
                {

                    if (!ConvertDateTime(viewModel))
                    {
                        return false;
                    }
                    else
                    {
                        ((CriteriaViewModel)(viewModel)).PrimeToForConversion();
                    }
                }

            }


            DateTimeBox timeBox = StringsToDateTimeBox(viewModel);

            if (timeBox == null)
            {
                return false;
            }


            if (MonthMatchesNumOfDays(timeBox))
            {
                viewModel.DateAndTime = timeBox.ConvertToDateTime();

                if (viewModel.DateAndTime > DateTime.Now)
                {
                    return false;
                }

                if (viewModel is CriteriaViewModel)
                {
                    if (((CriteriaViewModel)(viewModel)).FromDoesNotEqualsTo())
                    {
                        if (((CriteriaViewModel)(viewModel)).SecondPassConvertDateToTime == false)
                        {

                            ((CriteriaViewModel)(viewModel)).From = ((CriteriaViewModel)(viewModel)).DateAndTime;
                            ((CriteriaViewModel)(viewModel)).SecondPassConvertDateToTime = true;
                        }
                        else
                        {
                            ((CriteriaViewModel)(viewModel)).To = ((CriteriaViewModel)(viewModel)).DateAndTime;
                        }
                    }
                    else
                    {
                        ((CriteriaViewModel)(viewModel)).From = ((CriteriaViewModel)(viewModel)).DateAndTime;
                        ((CriteriaViewModel)(viewModel)).To = ((CriteriaViewModel)(viewModel)).DateAndTime;
                    }
                }

                return true;
            }
            return false;

        }

        public bool ConvertDateTime(TimeViewModel viewModel, Truck truck)
        {
            DateTimeBox timeBox = StringsToDateTimeBox(viewModel);

            if (timeBox == null)
            {
                return false;
            }


            if (MonthMatchesNumOfDays(timeBox))
            {
                truck.DateAndTime = timeBox.ConvertToDateTime();

                if (truck.DateAndTime > DateTime.Now)
                {
                    return false;
                }

                return true;
            }
            return false;

        }

        public DateTimeBox StringsToDateTimeBox(TimeViewModel viewModel)
        {

            if (viewModel.Year == null)
            {
                return null;
            }
            if (viewModel.Month == null)
            {
                return null;
            }
            if (viewModel.Day == null)
            {
                return null;
            }

            if (viewModel.Hour == null)
            {
                viewModel.Hour = "0";
            }
            if (viewModel.Minute == null)
            {
                viewModel.Minute = "0";
            }

            int year = 0;
            if (!(int.TryParse(viewModel.Year, out year)))
            {
                return null;
            }
            else if (year < 2018 || year > DateTime.Now.Year)
            {
                return null;
            }

            int month = 0;
            if (!(int.TryParse(viewModel.Month, out month)))
            {
                return null;
            }
            else if (month < 1 || month > 12)
            {
                return null;
            }

            int day = 0;
            if (!(int.TryParse(viewModel.Day, out day)))
            {
                return null;
            }
            else if (day < 1 || day > 31)
            {
                return null;
            }

            int hour = 0;
            if (!(int.TryParse(viewModel.Hour, out hour)))
            {
                return null;
            }
            else if (hour < 0 || hour > 23)
            {
                return null;
            }

            int minute = 0;
            if (!(int.TryParse(viewModel.Minute, out minute)))
            {
                return null;
            }
            else if (minute < 0 || minute > 59)
            {
                return null;
            }

            return new DateTimeBox(day, year, hour, month, minute);

        }


        public bool IsLeapYear(int year)
        {
            if (year % 4 == 0)
            {
                if (year % 100 == 0)
                {
                    if (!(year % 400 == 0))
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        public bool MonthMatchesNumOfDays(DateTimeBox timeBox)
        {
            int month = timeBox.Month;
            int day = timeBox.Day;
            int year = timeBox.Year;

            List<int> BigMonths = new List<int>();
            int[] arrayVar = { 1, 3, 5, 7, 8, 10, 12 };
            BigMonths.AddRange(arrayVar);

            if (BigMonths.Contains(month))
            {
                if (day <= 31)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (month != 2)
                {
                    if (day <= 30)
                    {
                        return true;
                    }
                }
                else
                {
                    if (IsLeapYear(year))
                    {
                        if (day <= 29)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (day <= 28)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }
    }
}
