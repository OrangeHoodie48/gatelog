﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Data;
using Microsoft.EntityFrameworkCore;
using GateLog.ViewModels;
using Microsoft.Extensions.Logging;
using System.IO;

namespace GateLog.Services
{
    public class DataService : IDataServices
    {
        GateLogDbContext context;
        IFilterMachine filterMachine;

        public DataService(GateLogDbContext context, IFilterMachine filterMachine)
        {
            this.context = context;
            this.filterMachine = filterMachine;
        }

        public Truck AddTruck(Truck truck)
        {
            context.Trucks.Add(truck);
            context.SaveChanges();
            if (truck.InOrOut == InOutEnum.In)
            {
                AddInTractor(truck);

                if (truck.Trailer != null)
                {
                    AddInTrailer(truck);
                }
            }
            else if (truck.InOrOut == InOutEnum.Out)
            {
                Carrier c = GetCarrierByName(truck.Carrier);
                foreach (InTractor t in context.InTractors)
                {

                    if (truck.Tractor == t.Tractor && c.HasCarrier(t.Carrier))
                    {
                        RemoveInTractor(t.Id);
                        break;
                    }
                }

                if (truck.Trailer != null)
                {
                    //List<Truck> otherTrucks = new List<Truck>();
                    bool trailerFound = false;
                    List<InTrailer> otherTrailers = new List<InTrailer>();
                    foreach (InTrailer tl in context.InTrailers)
                    {

                        if (truck.Trailer.Equals(tl.Trailer))
                        {
                            if (c.HasCarrier(tl.Carrier))
                            {
                                RemoveInTrailer(tl.Id);
                                trailerFound = true;
                                break;
                            }
                            else
                            {
                                otherTrailers.Add(tl);
                            }
                        }
                    }
                    if (trailerFound == false)
                    {
                        LogEntry log = new LogEntry();

                        log.DateAndTIme = truck.DateAndTime;
                        log.Text += "A trailer left that was not recorded entering";
                        log.Text += " The carrier taking the trailer was " +
                                truck.Carrier + " and the trailer number was " +
                                truck.Trailer + ".";

                        if (otherTrailers.Count > 0)
                        {

                            log.Text += " There are currently other trailer[s] with the " +
                                    "same number but different carrier in the lot. ";
                            foreach (InTrailer tl in otherTrailers)
                            {
                                log.Text += tl.Carrier + "-" + tl.Trailer + " ";

                            }
                        }
                        AddLogEntry(log);
                        foreach (InTrailer tl in otherTrailers)
                        {
                            if (tl.LogId == null)
                            {
                                tl.LogId = log.Id + "";
                            }
                            else
                            {
                                tl.LogId += "-" + log.Id;
                            }

                            
                                UpdateInTrailer(tl);
                            
                        }
                    }
                }
            }
            context.SaveChanges();
            return truck;
        }


        public List<Truck> GetAllTrucks()
        {
            List<Truck> trucks = context.Trucks.ToList();
            trucks = trucks.OrderBy(r => r.DateAndTime).ToList();
            return trucks;
        }

        public List<Truck> GetByTrip(string trip)
        {
            Truck t = context.Trucks.FirstOrDefault(r => r.HasTrip(trip));
            List<Truck> trucks = new List<Truck>();
            if (t != null)
            {
                trucks.Add(t);
            }
            return trucks;
        }

        public Truck GetById(int id)
        {
            Truck truck = GetAllTrucks().FirstOrDefault(r => r.Id == id);
            return truck;
        }

        public List<Truck> Get200From(int i)
        {
            List<Truck> allTrucks = GetAllTrucks().OrderByDescending(r => r.Id).ToList();
            if(allTrucks.Count == 0)
            {
                return allTrucks;
            }
            int allTrucksLength = allTrucks.Count;
            int i2 = 200;
            if ((allTrucksLength - 1) < i)
            {
                return null;
            }
            if (((allTrucksLength - 1) - i) < i2)
            {
                i2 = allTrucksLength - i;
            }
            return allTrucks.GetRange(i, i2);
        }

        public List<Truck> GetLastNTrucksFromList(int n, List<Truck> trucks)
        {
            int numOfTrucks = trucks.Count;

            if (numOfTrucks == 0)
            {
                return new List<Truck>();
            }

            if (numOfTrucks < n)
            {
                n = numOfTrucks;
            }

            trucks = trucks.GetRange(numOfTrucks - n, n);

            return trucks;
        }

        public Truck UpdateTruck(Truck truck)
        {
            context.Trucks.Attach(truck).State = EntityState.Modified;

            InTractor trac = GetInTractor(truck.Id);
            if (trac != null)
            {
                trac.Carrier = truck.Carrier;
                trac.DateAndTime = truck.DateAndTime;
                trac.Tractor = truck.Tractor;
                context.InTractors.Attach(trac).State = EntityState.Modified;
            }
            InTrailer trail = GetInTrailer(truck.Id);
            if (trail != null)
            {
                trail.Carrier = truck.Carrier;
                trail.DateAndTime = truck.DateAndTime;
                trail.Trailer = truck.Trailer;
                trail.LogId = truck.LogId;
                if (trail.ChangedEmptyStatus == false)
                {
                    if (truck.IsEmpty == true)
                    {
                        trail.Status = StatusEnum.Empty;
                    }
                    else if (truck.Trip != null)
                    {
                        trail.Status = StatusEnum.Inbound;
                    }
                    else if (truck.IsRelay)
                    {
                        trail.Status = StatusEnum.Relay;
                    }
                    else if (truck.InDropLot)
                    {
                        trail.Status = StatusEnum.Drop;
                    }
                    else
                    {
                        trail.Status = StatusEnum.None;
                    }
                }

                context.InTrailers.Attach(trail).State = EntityState.Modified;
            }
            context.SaveChanges();
            return truck;
        }

        public List<Truck> GetAllByCrieteriaChart(CriteriaChart criteria)
        {
            List<Truck> trucks = GetAllTrucks();
            trucks = filterMachine.FilterList(trucks, criteria);
            return trucks;
        }

        public bool AreValidTrips(string[] trips)
        {
            int numTrip = 0;
            foreach (string s in trips)
            {
                numTrip = 0;
                if (int.TryParse(s, out numTrip))
                {
                    if (numTrip < 980000)
                    {

                        return false;
                    }

                }
                else
                {
                    return false;
                }

            }
            return true;
        }

        public bool AreNonDuplicateTrips(string[] trips)
        {
            foreach (string trip in trips)
            {
                if (GetByTrip(trip).Count > 0)
                {
                    return false;
                }
            }

            return true;
        }

        public void FilterSeal(ViewModel inputModel)
        {
            if (inputModel.Seal != null)
            {
                if (inputModel.Seal.Trim().ToUpper().StartsWith("EMP"))
                {
                    inputModel.IsEmpty = true;
                    inputModel.IsRelay = false;
                    inputModel.InDropLot = false;
                    inputModel.Seal = "";
                }
                else if (inputModel.Seal.Trim().ToUpper().StartsWith("REL"))
                {
                    inputModel.IsRelay = true;
                    inputModel.IsEmpty = false;
                    inputModel.InDropLot = false;
                    inputModel.Seal = "";
                }
                else if (inputModel.Seal.Trim().ToUpper().StartsWith("DRO"))
                {
                    inputModel.InDropLot = true;
                    inputModel.IsRelay = false;
                    inputModel.IsEmpty = false;
                    inputModel.Seal = "";
                }
            }

        }

        public bool FilterDoor(ViewModel inputModel)
        {
            int n = 0;
            if (inputModel.Door != null)
            {
                if (inputModel.Door.Trim().ToUpper().StartsWith("Y"))
                {
                    inputModel.InYard = true;
                    inputModel.OnFence = false;
                    inputModel.Door = "";
                }

                else if (inputModel.Door.Trim().ToUpper().StartsWith("F"))
                {
                    inputModel.OnFence = true;
                    inputModel.InYard = false;
                    inputModel.Door = "";
                }
                else if (!(int.TryParse(inputModel.Door, out n)))
                {
                    return false;
                }
            }
            return true;
        }

        public Carrier GetCarrierById(int id)
        {
            return context.Carriers.FirstOrDefault(r => r.Id == id);
        }

        public Carrier GetCarrierByName(string name)
        {
            return context.Carriers.FirstOrDefault(r => r.HasCarrier(name) == true);
        }

        public Carrier AddCarrier(Carrier carrier)
        {
            string name = carrier.GetNames()[0];

            if (context.Carriers.Count() > 0)
            {
                if (context.Carriers.FirstOrDefault(r => r.HasCarrier(name) == true) != null)
                {
                    return null;
                }
                context.Carriers.Add(carrier);
                context.SaveChanges();
            }
            else
            {
                context.Carriers.Add(carrier);
                context.SaveChanges();
            }
            return carrier;
        }

        public Carrier RelateCarriers(Carrier car1, Carrier car2)
        {
            if (car1.Id == car2.Id)
            {
                return null;
            }
            else
            {
                car1 = GetCarrierById(car1.Id);
            }
            foreach (string name in car2.GetNames())
            {
                if (car1.GetNames().FirstOrDefault(r => r.Equals(name)) == null)
                {
                    car1.AddCarrierName(name);
                }
            }
            context.Carriers.Remove(car2);
            context.Carriers.Attach(car1).State = EntityState.Modified;
            context.SaveChanges();

            return car1;
        }

        public List<Carrier> GetAllCarriers()
        {
            return context.Carriers.ToList();
        }

        public List<string> GetAllCarrierNames()
        {
            List<string> allNames = new List<string>();
            foreach (Carrier c in context.Carriers)
            {
                allNames.AddRange(c.GetNames());
            }

            if (allNames.Count > 0)
            {
                allNames.Sort();
            }
            return allNames;
        }

        public bool AreSameCarrier(string name1, string name2)
        {
            bool results = false;
            foreach (Carrier c in context.Carriers)
            {
                if (c.AreRelatedCarrierNames(name1, name2))
                {
                    results = true;
                }
            }
            return results;
        }

        public List<string> GetAllNamesForCarrier(Carrier c)
        {
            return c.GetNames();
        }

        public Carrier RemoveCarrier(Carrier c)
        {
            context.Carriers.Remove(c);
            context.SaveChanges();
            return c;
        }


        public void RemoveCarrierName(string name)
        {
            foreach (Carrier c in context.Carriers)
            {
                if (c.HasCarrier(name))
                {
                    c.RemoveCarrierName(name);
                    break;
                }
            }
            context.SaveChanges();
        }

        public List<string> GetRelatedCarriersFromName(string name)
        {
            List<string> names = new List<string>();
            foreach (Carrier c in context.Carriers)
            {
                if (c.HasCarrier(name))
                {
                    names = c.GetNames();
                    names.Remove(name);
                    break;
                }
            }
            if (names.Count == 0)
            {
                names.Add("No other carrier names have been associated with " + name + " yet.");
            }
            return names;

        }
        public List<string> GetPotentialRelatableCarriers(Carrier c)
        {
            List<string> names = GetAllCarrierNames();
            List<string> currentlyRelated = GetRelatedCarriersFromName(c.GetNames()[0]);
            names.RemoveAll(r => currentlyRelated.Contains(r));
            names.Remove(c.GetNames()[0]);
            return names;
        }
        public InTractor GetInTractor(int entryId)
        {
            InTractor trac = null;
            foreach (InTractor t in context.InTractors)
            {
                if (t.EntryId == entryId)
                {
                    trac = t;
                    break;
                }
            }
            return trac;
        }
        public InTrailer GetInTrailer(int entryId)
        {
            InTrailer trail = null;
            foreach (InTrailer t in context.InTrailers)
            {
                if (t.EntryId == entryId)
                {
                    trail = t;
                    break;
                }
            }
            return trail;
        }
        public InTractor AddInTractor(Truck t)
        {
            InTractor trac = null;
            if (GetInTractor(t.Id) == null)
            {
                trac = new InTractor();
                trac.EntryId = t.Id;
                trac.Carrier = t.Carrier;
                trac.DateAndTime = t.DateAndTime;
                trac.Tractor = t.Tractor;
                context.InTractors.Add(trac);
                context.SaveChanges();
            }
            return trac;
        }
        public InTractor AddInTractor(InTractor t)
        {
            context.InTractors.Add(t);
            context.SaveChanges();
            return t; 
        }
        public InTrailer AddInTrailer(InTrailer t)
        {
            context.InTrailers.Add(t);
            context.SaveChanges();
            return t;
        }
        public InTrailer AddInTrailer(Truck t)
        {
            InTrailer trail = null;
            if (GetInTrailer(t.Id) == null)
            {
                trail = new InTrailer();
                trail.EntryId = t.Id;
                trail.DateAndTime = t.DateAndTime;
                trail.Carrier = t.Carrier;
                trail.Trailer = t.Trailer;
                trail.LogId = t.LogId;
                
                if(t.Trip != null)
                {
                    trail.Status = StatusEnum.Inbound;
                }
                else if (t.IsEmpty)
                {
                    trail.Status = StatusEnum.Empty;
                }
                else if (t.IsRelay)
                {
                    trail.Status = StatusEnum.Relay;
                }
                else if (t.InDropLot)
                {
                    trail.Status = StatusEnum.Drop;
                }
                else
                {
                    trail.Status = StatusEnum.None;
                }
                context.InTrailers.Add(trail);
                context.SaveChanges();
            }
            return trail;
        }
        public InTractor RemoveInTractor(int id)
        {
            InTractor trac = context.InTractors.FirstOrDefault(r => r.Id == id);
            if (trac != null)
            {
                context.InTractors.Remove(trac);
                context.SaveChanges();
            }
            return trac;
        }
        public InTrailer RemoveInTrailer(int id)
        {
            InTrailer trail = context.InTrailers.FirstOrDefault(r => r.Id == id);
            if (trail != null)
            {
                context.InTrailers.Remove(trail);
                context.SaveChanges();
            }
            return trail;
        }
        public InTractor RemoveInTractorFromEntryId(int entryId)
        {
            InTractor trac = context.InTractors.FirstOrDefault(r => r.EntryId == entryId);
            if (trac != null)
            {
                context.InTractors.Remove(trac);
                context.SaveChanges();
            }
            return trac;
        }
        public InTrailer RemoveInTrailerFromEntryId(int entryId)
        {
            InTrailer trail = context.InTrailers.FirstOrDefault(r => r.EntryId == entryId);
            if (trail != null)
            {
                context.InTrailers.Remove(trail);
                context.SaveChanges();
            }
            return trail;
        }

        public List<InTractor> GetAllInTractors()
        {
            return context.InTractors.ToList();
        }

        public List<string> GetAllInTractorNumbersOfCarrier(string carrierName)
        {
            Carrier c = GetCarrierByName(carrierName);
            List<string> inTractNums = new List<string>();
            List<InTractor> all = GetAllInTractors();
            foreach (InTractor iT in all)
            {
                if (c.HasCarrier(iT.Carrier))
                {
                    inTractNums.Add(iT.Tractor);
                }
            }
            return inTractNums;
        }

        public List<InTrailer> GetAllInTrailers()
        {
            return context.InTrailers.ToList();
        }

        public InTrailer UpdateInTrailer(InTrailer inTrailer)
        {

            context.InTrailers.Attach(inTrailer).State = EntityState.Modified;
            return inTrailer;

        }

        public LogEntry AddLogEntry(LogEntry log)
        {
            context.LogEntries.Add(log);
            context.SaveChanges();
            return log;
        }

        public List<LogEntry> GetAllLogEntries()
        {
            return context.LogEntries.OrderByDescending(r => r.Id).ToList();
        }

        public InTrailer ToggleFilledStatus(int id)
        {
            InTrailer t = context.InTrailers.FirstOrDefault(r => r.Id == id);
            if (t != null)
            {
                if (t.ChangedEmptyStatus == false)
                {
                    t.ChangedEmptyStatus = true;
                }
                if(t.Status == StatusEnum.Empty)
                {
                    t.Status = StatusEnum.Inbound;
                }
                else
                {
                    t.Status = StatusEnum.Empty;
                }

            context.InTrailers.Attach(t).State = EntityState.Modified;
            context.SaveChanges();
            }
            return t; 
            
    }
        public List<string> GetFirstCarriers()
        {
            List<string> firstCarriers = new List<string>(); 
            foreach(Carrier c in GetAllCarriers())
            {
                firstCarriers.Add(c.GetNames()[0]);
            }
            return firstCarriers; 
        }


        public  Dictionary<string, List<InTrailer>> GetCarrierTrailerMap()
        {
            Dictionary<string, List<InTrailer>> carrierTrailerMap = new Dictionary<string, List<InTrailer>>();

            List<InTrailer> inTrailers = GetAllInTrailers(); 
            foreach(InTrailer t in inTrailers)
            {
                Carrier c = GetCarrierByName(t.Carrier);
                string firstCarrier = c.GetNames()[0];
                if (!carrierTrailerMap.Keys.Contains(firstCarrier))
                {
                    carrierTrailerMap.Add(firstCarrier, new List<InTrailer>());
                }
                carrierTrailerMap[firstCarrier].Add(t);
            }
            return carrierTrailerMap; 
        }

        public Dictionary<string, List<InTractor>> GetCarrierTractorMap()
        {
            Dictionary<string, List<InTractor>> carrierTractorMap = new Dictionary<string, List<InTractor>>();
            List<InTractor> inTractors = new List<InTractor>(); 
            foreach (InTractor t in inTractors)
            {
                Carrier c = GetCarrierByName(t.Carrier);
                string firstCarrier = c.GetNames()[0];
                if (!carrierTractorMap.Keys.Contains(firstCarrier))
                {
                    carrierTractorMap.Add(firstCarrier, new List<InTractor>());
                }
                carrierTractorMap[firstCarrier].Add(t);
            }
            return carrierTractorMap;
        }


        public Dictionary<string, string> GetRelatedToFirstCarrierMap()
        {
            Dictionary<string, string> relatedToFirstCarrierMap = new Dictionary<string, string>(); 
            foreach(Carrier c in GetAllCarriers())
            {
                string firstCarrier = c.GetNames()[0];
                foreach(string s in c.GetNames())
                {
                    relatedToFirstCarrierMap.Add(s, firstCarrier); 
                }
            }
            return relatedToFirstCarrierMap; 
        }
        
        public void RemoveTruck(int id)
        {
            context.Trucks.Remove(context.Trucks.FirstOrDefault(r => r.Id == id));
            context.SaveChanges();
        }

        public void RemoveLogEntry(int id)
        {
            context.LogEntries.Remove(context.LogEntries.FirstOrDefault(r => r.Id == id));
            context.SaveChanges();
        }

    }
}
