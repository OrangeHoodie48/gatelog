﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Data;
using GateLog.Delegates.Tools;

namespace GateLog.Services
{
    public interface IFilterMachine
    {

        List<Truck> FilterList(List<Truck> trucks, CriteriaChart criteria);

    }
}
