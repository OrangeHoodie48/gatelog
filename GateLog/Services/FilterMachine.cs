﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GateLog.Models;
using GateLog.Data;
using GateLog.Delegates;
using GateLog.Delegates.Tools;
using System.IO;

namespace GateLog.Services
{
    public class FilterMachine : IFilterMachine
    {


        public List<Truck> FilterList(List<Truck> trucks, CriteriaChart criteria)
        {
            List<Truck> results = new List<Truck>();
            IsMatchDelegate isMatch = BuildDelegate(criteria);
            BoolBox box = new BoolBox();
   
            

            foreach (Truck t in trucks)
            {
                box.TruthValue = true;
                isMatch(t, criteria, box);
                if (box.TruthValue == true)
                {
                    results.Add(t);
                }

            }
            return results;
        }

        private IsMatchDelegate BuildDelegate(CriteriaChart criteria)
        {
            IsMatchDelegate isMatch = new IsMatchDelegate(IsInTimePeriord);

            if (criteria.From == null)
            {
                isMatch = null;
            }

            if (criteria.Tractor != null)
            {
                isMatch += HasSameTractor;
            }

            if (criteria.Trailer != null)
            {
                isMatch += HasSameTrailer;
            }

            if (criteria.Carrier != null)
            {
                isMatch += HasSameCarrier;
            }

            if(criteria.Company != SearchPageCompanyEnum.SELECT)
            {
                isMatch += IsSameCompany; 
            }

            return isMatch;
        }

        private BoolBox IsInTimePeriord(Truck t, CriteriaChart c, BoolBox box)
        {
            if (!(t.DateAndTime >= c.From && t.DateAndTime <= c.To))
            {
                box.TruthValue = false;
            }

            return box;
        }

        private BoolBox HasSameTractor(Truck t, CriteriaChart c, BoolBox box)
        {
            if (t.Tractor != c.Tractor)
            {
                box.TruthValue = false;
            }

            return box;
        }


        private BoolBox HasSameTrailer(Truck t, CriteriaChart c, BoolBox box)
        {
            if (t.Trailer == null)
            {
                box.TruthValue = false;
                return box;
            }

            if (!t.Trailer.Equals(c.Trailer))
            {
                box.TruthValue = false;
            }

            return box;
        }


        private BoolBox HasSameCarrier(Truck t, CriteriaChart c, BoolBox box)
        {

            if (!c.ActualCarrierObject.HasCarrier(t.Carrier))
            {
                box.TruthValue = false;
            }

            return box;
        }

        private BoolBox IsSameCompany(Truck t, CriteriaChart c, BoolBox box)
        {
            if(c.Company == SearchPageCompanyEnum.SELECT)
            {
                return box;
            }
            else if(c.Company == SearchPageCompanyEnum.ESSENDANT && t.Company != CompanyEnum.ESSENDANT)
            {
                box.TruthValue = false;
            }
            else if (c.Company == SearchPageCompanyEnum.NCS && t.Company != CompanyEnum.NCS)
            {
                box.TruthValue = false;
            }
            else if (c.Company == SearchPageCompanyEnum.OTHER && t.Company != CompanyEnum.NONE)
            {
                box.TruthValue = false;
            }


            return box;
        }
    }
}
