﻿using GateLog.Models;
using GateLog.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GateLog.Services
{
    public interface IDateTimeService
    {
        bool ConvertDateTime(TimeViewModel viewModel, Truck truck);
        DateTimeBox StringsToDateTimeBox(TimeViewModel viewModel);
        bool IsLeapYear(int year);
        bool MonthMatchesNumOfDays(DateTimeBox timeBox);
        bool ConvertDateTime(TimeViewModel viewModel);

    }
}
